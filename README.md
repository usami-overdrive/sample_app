# Ruby on Railsチュートリアル: サンプルアプリケーション

https://usami-overdrive-sample.herokuapp.com/

これは以下に基づいたサンプル・アプリケーションです<br>
[*Ruby on Railsチュートリアル:
実例を使ってRailsを学ぼう*](http://railstutorial.jp/)
[Michael Hartl](http://www.michaelhartl.com/)著